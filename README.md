# ProjetGit

## Getting Started

This project use npm ([NodeJS](https://nodejs.org/en/)).

1. If not already done, [install NodeJS](https://nodejs.org/en/download/)
2. Run `npm install` to install dependencies

#### Linter

The linter is using [Husky](https://typicode.github.io/husky/#/), [lint-staged](https://github.com/okonet/lint-staged), [ESLint](https://eslint.org/), [Prettier](https://prettier.io/).

[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-2.1-4baaaa.svg)](CODE_OF_CONDUCT.md)
[![Github](https://img.shields.io/badge/GitHub-100000?style=for-the-badge&logo=github&logoColor=white)](https://github.com/Benoitapps/ProjetGit)
[![Gitlab](https://img.shields.io/badge/GitLab-330F63?style=for-the-badge&logo=gitlab&logoColor=white)](https://gitlab.com/alvauthier/ProjetGit)
