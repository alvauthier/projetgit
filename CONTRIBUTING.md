# Contributing to ProjetGit

:+1::tada: Thanks for checking out ProjetGit! We're excited to hear and learn from you. :tada::+1:

We've put together the following guidelines to help you figure out where you can best be helpful.

## Table of Contents

0. [Types of contributions we're looking for](#types-of-contributions-were-looking-for)
1. [Ground rules & expectations](#ground-rules--expectations)
2. [How to contribute](#how-to-contribute)
3. [Style guide](#style-guide)
4. [Setting up your environment](#setting-up-your-environment)
5. [Community](#community)

## Types of contributions we're looking for

There are many ways you can directly contribute to the guides (in descending order of need):

- Fix editorial inconsistencies or inaccuracies
- Translate guides into other languages

Interested in contributing to ProjetGit? Read on!

## Ground rules & expectations

Before we get started, here are a few things we expect from you (and that you should expect from others):

- Be kind and thoughtful in your conversations around this project. We all come from different backgrounds and projects, which means we likely have different perspectives on "how open source is done." Try to listen to others rather than convince them that your way is correct.
- ProjetGit is released with a [Contributor Code of Conduct](./CODE_OF_CONDUCT.md). By participating in this project, you agree to abide by its terms.
- Please ensure that your contribution passes all tests if you open a pull request. If there are test failures, you will need to address them before we can merge your contribution.
- When adding content, please consider if it is widely valuable. Please don't add references or links to things you or your employer have created, as others will do so if they appreciate it.

## How to contribute

If you'd like to contribute, start by searching through the [pull requests](https://github.com/Benoitapps/ProjetGit/pulls) to see whether someone else has raised a similar idea or question.

If you don't see your idea listed, and you think it fits into the goals of this guide, open a pull request.

## Style guide

### Mentions

When referring to people that use GitHub, use @mentions of their username instead of their full name.

- :smile: As @jessfraz put it...
- :cry: As [Jess Frazelle](https://github.com/jessfraz) put it...

When referring to a project on GitHub, link to the repository so others can dive deeper, if they choose.

- :smile: @maxogden took a similar approach to [Dat](https://github.com/datproject/dat)...
- :cry: @maxogden took a similar approach to Dat...

## Setting up your environment

Please refer to the [README](./README.md) of the project.

This project use npm ([NodeJS](https://nodejs.org/en/)).

1. If not already done, [install NodeJS](https://nodejs.org/en/download/)
2. Run `npm install` to install dependencies

Open index.html in your web browser.

## Community

Discussions about ProjetGit take place on this repository's [Issues](https://github.com/Benoitapps/ProjetGit/issues) and [Pull Requests](https://github.com/Benoitapps/ProjetGit/pulls) sections. Anybody is welcome to join these conversations.

Wherever possible, do not take these conversations to private channels, including contacting the maintainers directly. Keeping communication public means everybody can benefit and learn from the conversation.
